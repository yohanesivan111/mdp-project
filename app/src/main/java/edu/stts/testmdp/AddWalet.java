package edu.stts.testmdp;

import android.arch.persistence.room.Room;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.List;

public class AddWalet extends AppCompatActivity implements WalletAdapter.OnWalletListener {

    RecyclerView recyclerView;
    RecyclerView.Adapter adapter;
    Button btnadd;
    EditText ednama, edjumlah;
    List<Wallet> wallets;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_walet);

        ednama = findViewById(R.id.ed_wname);
        edjumlah = findViewById(R.id.ed_wamount);
        btnadd = findViewById(R.id.bt_add_wallet);
        final DbWallet db = Room.databaseBuilder(getApplicationContext(), DbWallet.class, "dbwallet").allowMainThreadQueries().build();

        recyclerView = findViewById(R.id.rviewwallet);

        wallets = db.walletDao().getAllWallet();

        if(wallets.size() > 0)
        {
            recyclerView.setLayoutManager(new LinearLayoutManager(this));
            adapter = new WalletAdapter(wallets,  this);
            recyclerView.setAdapter(adapter);
        }

        btnadd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                db.walletDao().insertAll(new Wallet(ednama.getText().toString(),LoginActivity.userLogin,Integer.parseInt(edjumlah.getText().toString())));

                wallets.clear();

                wallets = db.walletDao().getAllWallet();
                recyclerView.setAdapter(adapter);

                ednama.setText("");
                edjumlah.setText("");
                adapter.notifyDataSetChanged();

                Toast.makeText(getApplicationContext(), "Success adding wallet!", Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public void onWalletClick(int position) {
        MainActivity.walletid = wallets.get(position).getId_wallet();
        MainActivity.namawallet = wallets.get(position).getName();
        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra("walletid",wallets.get(position).getId_wallet());
        startActivity(intent);
    }
}
