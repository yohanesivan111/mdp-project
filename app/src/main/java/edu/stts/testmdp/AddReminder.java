package edu.stts.testmdp;

import android.arch.persistence.room.Room;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Arrays;

public class AddReminder extends AppCompatActivity {
    EditText ed_reminder_name, ed_reminder_date;
    Button btn_add_reminder;

    ListView lv1;

    ArrayList<Reminder> data = new ArrayList<Reminder>();
    ArrayAdapter<Reminder> myadapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_reminder);
        ed_reminder_name = findViewById(R.id.ed_reminder_name);
        ed_reminder_date = findViewById(R.id.ed_reminder_date);
        lv1 = findViewById(R.id.lsview1);
        btn_add_reminder = findViewById(R.id.btn_add_duration);
        final DbReminder db = Room.databaseBuilder(getApplicationContext(), DbReminder.class, "dbreminderr").allowMainThreadQueries().build();

        if (db.reminderDao().getItemCount() > 0)
        {
            data.addAll(Arrays.asList(db.reminderDao().getAllReminder()));
            myadapter = new ArrayAdapter<Reminder>(this, android.R.layout.simple_list_item_1, data);
            lv1.setAdapter(myadapter);
        }

        btn_add_reminder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                db.reminderDao().insertAll(new Reminder(ed_reminder_name.getText().toString(), ed_reminder_date.getText().toString(), LoginActivity.userLogin));
                myadapter.notifyDataSetChanged();
            }
        });
    }


}
