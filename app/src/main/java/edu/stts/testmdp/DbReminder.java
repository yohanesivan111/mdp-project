package edu.stts.testmdp;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

@Database(entities = {Reminder.class}, version = 1)
public abstract class DbReminder extends RoomDatabase {
    public abstract ReminderDao reminderDao();
}
