package edu.stts.testmdp;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

@Entity
public class Transaction {
    public Transaction(int jumlah, String tipe, String desc, int idwallet) {
        this.jumlah = jumlah;
        this.tipe = tipe;
        this.desc = desc;
        this.idwallet = idwallet;
    }

    @PrimaryKey(autoGenerate = true)
    private int id;

    public void setId(int id) {
        this.id = id;
    }

    public void setIdwallet(int idwallet) {
        this.idwallet = idwallet;
    }

    @ColumnInfo(name = "jumlah")
    private int jumlah;

    @ColumnInfo(name = "idwallet")
    private int idwallet;

    @ColumnInfo(name = "tipe")
    private String tipe;

    @ColumnInfo(name = "desc")
    private String desc;

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public int getId() {
        return id;
    }

    public int getJumlah() {
        return jumlah;
    }

    public void setJumlah(int jumlah) {
        this.jumlah = jumlah;
    }

    public int getIdwallet() {
        return idwallet;
    }

    public String getTipe() {
        return tipe;
    }

    public void setTipe(String tipe) {
        this.tipe = tipe;
    }
}
