package edu.stts.testmdp;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

@Dao
public interface BudgetDao {
    @Query("SELECT * from Budget")
    List<Budget> getAllBudget();

    @Insert
    void insertAll(Budget... budgets);
}
