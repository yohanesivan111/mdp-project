package edu.stts.testmdp;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

@Entity
public class Budget {

    @PrimaryKey(autoGenerate = true)
    private int id_budget;

    @ColumnInfo(name = "max_amount")
    private int max_amount;

    @ColumnInfo(name = "curr_amount")
    private int curr_amount;

    @ColumnInfo(name = "walletid")
    private int walletid;

    @ColumnInfo(name = "username")
    private String username;

    @ColumnInfo(name = "namawallet")
    private String namawallet;

    public Budget(int max_amount, int curr_amount, int walletid, String username, String namawallet) {
        this.max_amount = max_amount;
        this.curr_amount = curr_amount;
        this.walletid = walletid;
        this.username = username;
        this.namawallet = namawallet;
    }

    public String getNamawallet() {
        return namawallet;
    }

    public void setNamawallet(String namawallet) {
        this.namawallet = namawallet;
    }

    public int getId_budget() {
        return id_budget;
    }

    public void setId_budget(int id_budget) {
        this.id_budget = id_budget;
    }

    public int getMax_amount() {
        return max_amount;
    }

    public void setMax_amount(int max_amount) {
        this.max_amount = max_amount;
    }

    public int getCurr_amount() {
        return curr_amount;
    }

    public void setCurr_amount(int curr_amount) {
        this.curr_amount = curr_amount;
    }

    public int getWalletid() {
        return walletid;
    }

    public void setWalletid(int walletid) {
        this.walletid = walletid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
