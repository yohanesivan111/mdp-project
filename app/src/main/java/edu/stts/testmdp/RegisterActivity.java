package edu.stts.testmdp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

public class RegisterActivity extends AppCompatActivity {
    EditText ed_nama, ed_user, ed_pass, ed_cpass, ed_phone;
    ImageView btn_back;
    Button bt_regis, bt_cancel;

    RequestQueue rq;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        getSupportActionBar().hide();

        rq = Volley.newRequestQueue(this);

        btn_back = findViewById(R.id.imgLogo);
        ed_nama = findViewById(R.id.ed_name);
        ed_cpass = findViewById(R.id.ed_cpassword);
        ed_pass = findViewById(R.id.ed_password);
        ed_user = findViewById(R.id.ed_username);
        ed_phone = findViewById(R.id.ed_phone);
        bt_regis = findViewById(R.id.btn_register);
        bt_cancel = findViewById(R.id.btn_cancel);

        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(RegisterActivity.this, LoginActivity.class));
            }
        });

        bt_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(RegisterActivity.this, LoginActivity.class));
            }
        });

        bt_regis.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(ed_cpass.getText().toString().equals(ed_pass.getText().toString()))
                {
                    String url = "http://soa.sensecode.id/user/register/5d0ce2f1f3ad4365a62c93f4fc8bbefa";
                    JSONObject data = new JSONObject();

                    try {
                        data.put("username",ed_user.getText().toString());
                        data.put("name", ed_nama.getText().toString());
                        data.put("phone", ed_phone.getText().toString());
                        data.put("password", ed_pass.getText().toString());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    JsonObjectRequest req = new JsonObjectRequest(
                            Request.Method.POST, url, data,
                            new Response.Listener<JSONObject>() {
                                @Override
                                public void onResponse(JSONObject response) {
                                    try {
                                        if (response.getString("status").equals("true"))
                                        {
                                            startActivity(new Intent(RegisterActivity.this, LoginActivity.class));
                                            Toast.makeText(getApplicationContext(), "Registration Successful please confirm your email", Toast.LENGTH_LONG).show();
                                        }else{
                                            Toast.makeText(getApplicationContext(), response.getString("status "), Toast.LENGTH_LONG).show();
                                        }
                                    } catch (JSONException e) {
                                        Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                                    }
                                }
                            }, new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_LONG).show();
                                }
                            }
                    );

                    rq.add(req);
                }else{
                    Toast.makeText(getApplicationContext(), "Password Harus Sama", Toast.LENGTH_LONG).show();
                }

            }
        });

    }
}
