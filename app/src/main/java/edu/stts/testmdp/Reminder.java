package edu.stts.testmdp;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

@Entity
public class Reminder {

    @PrimaryKey(autoGenerate = true)
    private int id_reminder;

    @ColumnInfo(name = "judul_reminder")
    private String judul_reminder;

    @ColumnInfo(name = "date")
    private String date;

    @ColumnInfo(name = "username")
    private String username;

    public Reminder(String judul_reminder, String date, String username) {
        this.judul_reminder = judul_reminder;
        this.date = date;
        this.username = username;
    }

    public int getId_reminder() {
        return id_reminder;
    }

    public void setId_reminder(int id_reminder) {
        this.id_reminder = id_reminder;
    }

    public String getJudul_reminder() {
        return judul_reminder;
    }

    public void setJudul_reminder(String judul_reminder) {
        this.judul_reminder = judul_reminder;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public String toString() {
        return "Reminder{" +
                "Judul='" + judul_reminder + "\n" +
                ", Date='" + date + "\n" +
                '}';
    }
}
