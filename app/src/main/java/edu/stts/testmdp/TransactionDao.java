package edu.stts.testmdp;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

@Dao
public interface TransactionDao {
    @Query("SELECT * from `Transaction` where idwallet= :idwallet ")
    List<Transaction> getAllTransaction(int idwallet);

    @Insert
    void insertAll(Transaction... transactions);

    @Query("DELETE FROM `Transaction`")
    void deleteAll();
}