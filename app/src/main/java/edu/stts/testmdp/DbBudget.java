package edu.stts.testmdp;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

@Database(entities = {Budget.class}, version = 1)
public abstract class DbBudget extends RoomDatabase {
    public abstract BudgetDao budgetDao();
}
