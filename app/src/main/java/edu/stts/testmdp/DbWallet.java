package edu.stts.testmdp;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

@Database(entities = {Wallet.class}, version = 1)
public abstract class DbWallet extends RoomDatabase {
    public abstract WalletDao walletDao();
}
