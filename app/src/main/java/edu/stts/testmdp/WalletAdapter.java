package edu.stts.testmdp;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

class WalletAdapter extends RecyclerView.Adapter<WalletAdapter.ViewHolder> {
    List<Wallet> wallets;
    private OnWalletListener mOnWalletListener;

    public WalletAdapter(List<Wallet> wallets, OnWalletListener onWalletListener) {
        this.wallets = wallets;
        this.mOnWalletListener = onWalletListener;
    }

    @NonNull
    @Override
    public WalletAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.transaction_row, viewGroup, false);
        return new ViewHolder(view, mOnWalletListener);
    }

    @Override
    public void onBindViewHolder(@NonNull WalletAdapter.ViewHolder viewHolder, int i) {
        viewHolder.tx_tipe.setText(wallets.get(i).getAmount()+"");
        viewHolder.tx_desc.setText(wallets.get(i).getName());
    }

    @Override
    public int getItemCount() {
        return wallets.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView tx_desc, tx_tipe, tx_jumlah;
        OnWalletListener onWalletListener;
        public ViewHolder(@NonNull View itemView,OnWalletListener onWalletListener) {
            super(itemView);
            tx_desc = itemView.findViewById(R.id.tx_desc);
            tx_tipe = itemView.findViewById(R.id.tx_tipe);
            tx_jumlah = itemView.findViewById(R.id.tx_jumlah);
            this.onWalletListener = onWalletListener;
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            onWalletListener.onWalletClick(getAdapterPosition());
        }
    }

    public interface OnWalletListener{
        void onWalletClick(int position);
    }
}