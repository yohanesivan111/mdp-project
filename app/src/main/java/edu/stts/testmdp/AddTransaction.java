package edu.stts.testmdp;

import android.arch.persistence.room.Room;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;

public class AddTransaction extends AppCompatActivity {

    EditText ed_jum, ed_desc;
    Spinner sp_tipe, sp_tipe1;
    Button bt_add;
    RequestQueue rq;

    private static final String TAG = "AddTransaction";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_transaction);
        setTitle("Add Transaction");

        rq = Volley.newRequestQueue(this);

        ed_jum = findViewById(R.id.np_jum);
        sp_tipe = findViewById(R.id.sp_tipe);
        bt_add = findViewById(R.id.bt_add);
        ed_desc = findViewById(R.id.ed_desc);
        sp_tipe1 = findViewById(R.id.sp_tipe1);

        final AppDatabase db = Room.databaseBuilder(getApplicationContext(), AppDatabase.class, "production").allowMainThreadQueries().build();

        bt_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int jumlah = Integer.parseInt(ed_jum.getText().toString());
                if (sp_tipe1.getSelectedItem().toString().equalsIgnoreCase("expenses"))
                {
                    jumlah *= -1;
                }
                //SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd");
                //String currentDateandTime = sdf.format(new Date());
                db.transactionDao().insertAll(new Transaction(jumlah, sp_tipe.getSelectedItem().toString(), ed_desc.getText().toString(), MainActivity.walletid));
                String url = "http://soa.sensecode.id/cashflow/insert/5d0ce2f1f3ad4365a62c93f4fc8bbefa";
                JSONObject data = new JSONObject();

                try {
                    data.put("id_wallet","1");
                    data.put("ammount", jumlah);
                    data.put("notes", ed_desc.getText().toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                JsonObjectRequest req = new JsonObjectRequest(
                        Request.Method.POST, url, data,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                try {
                                    if (response.getString("status").equals("true"))
                                    {
                                        startActivity(new Intent(AddTransaction.this, MainActivity.class));
                                    }else{
                                        Toast.makeText(getApplicationContext(), response.getString("status"), Toast.LENGTH_LONG).show();

                                    }
                                } catch (JSONException e) {
                                    Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                                }
                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }
                );

                rq.add(req);
            }
        });
    }
}
