package edu.stts.testmdp;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.List;

class BudgetAdapter extends RecyclerView.Adapter<BudgetAdapter.ViewHolder> {
    List<Budget> budgets;

    public BudgetAdapter(List<Budget> budgets) {
        this.budgets = budgets;
    }

    @NonNull
    @Override
    public BudgetAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.budget_row, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull BudgetAdapter.ViewHolder viewHolder, int i) {
        //viewHolder.tx_date.setText(transactions.get(i).getDatetime().toString());
        viewHolder.tx_amount.setText(budgets.get(i).getCurr_amount()+"");
        viewHolder.tx_namawallet.setText(budgets.get(i).getNamawallet());
        viewHolder.tx_max_ammount.setText(budgets.get(i).getMax_amount()+"");
        int ammount = budgets.get(i).getCurr_amount();
        int maxammount = budgets.get(i).getMax_amount();
        viewHolder.pgbar.setProgress((ammount/maxammount)*100);
    }

    @Override
    public int getItemCount() {
        return budgets.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tx_amount, tx_max_ammount, tx_namawallet;
        ProgressBar pgbar;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tx_amount = itemView.findViewById(R.id.tx_amoung);
            tx_max_ammount = itemView.findViewById(R.id.tx_max_budget);
            tx_namawallet = itemView.findViewById(R.id.tx_nama_wallet);
            pgbar = itemView.findViewById(R.id.pg_bar);
        }
    }
}
