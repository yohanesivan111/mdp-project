package edu.stts.testmdp;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

@Dao
public interface WalletDao {
    @Query("SELECT * from `Wallet`")
    List<Wallet> getAllWallet();

    @Insert
    void insertAll(Wallet... wallets);

    @Query("DELETE FROM `wallet`")
    void deleteAll();

    @Query("UPDATE `wallet` set amount= :jumlah where id_wallet= :walletid ")
    void updateWallet(int walletid, int jumlah);
}