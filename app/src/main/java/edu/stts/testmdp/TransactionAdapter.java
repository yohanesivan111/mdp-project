package edu.stts.testmdp;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

class TransactionAdapter extends RecyclerView.Adapter<TransactionAdapter.ViewHolder> {
    List<Transaction> transactions;

    public TransactionAdapter(List<Transaction> transactions) {
        this.transactions = transactions;
    }

    @NonNull
    @Override
    public TransactionAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.transaction_row, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull TransactionAdapter.ViewHolder viewHolder, int i) {
        viewHolder.tx_tipe.setText(transactions.get(i).getTipe().toString());
        viewHolder.tx_desc.setText(transactions.get(i).getDesc().toString());
        viewHolder.tx_jumlah.setText("Rp " +transactions.get(i).getJumlah()+"");
        //viewHolder.tx_date.setText(transactions.get(i).getDatetime().toString());
    }

    @Override
    public int getItemCount() {
        return transactions.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tx_desc, tx_tipe, tx_jumlah, tx_date;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tx_desc = itemView.findViewById(R.id.tx_desc);
            tx_tipe = itemView.findViewById(R.id.tx_tipe);
            tx_jumlah = itemView.findViewById(R.id.tx_jumlah);
            //tx_date = itemView.findViewById(R.id.tx_date);
        }
    }
}
