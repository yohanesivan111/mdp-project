package edu.stts.testmdp;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.ArrayList;

@Dao
public interface ReminderDao {
    @Query("SELECT * from Reminder")
    Reminder[] getAllReminder();

    @Insert
    void insertAll(Reminder... reminders);

    @Query("SELECT COUNT(*) from Reminder")
    int getItemCount();
}