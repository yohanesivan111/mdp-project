package edu.stts.testmdp;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

public class LoginActivity extends AppCompatActivity {
    RelativeLayout rrelay1, rrelay2;

    EditText eduser, edpass;
    public static String userLogin;
    RequestQueue rq;

    Handler handler = new Handler();
    Runnable runnable = new Runnable() {
        @Override
        public void run() {
            rrelay1.setVisibility(View.VISIBLE);
            rrelay2.setVisibility(View.VISIBLE);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        rrelay1 = findViewById(R.id.rellay1);
        rrelay2 = findViewById(R.id.rellay2);

        eduser = findViewById(R.id.ed_username_login);
        edpass = findViewById(R.id.ed_password_login);

        handler.postDelayed(runnable, 2000);

        rq = Volley.newRequestQueue(this);
    }

    public void btnSignupClick(View v)
    {
        startActivity(new Intent(LoginActivity.this, RegisterActivity.class));
    }

    public void btnLoginClick(View v)
    {
        //startActivity(new Intent(LoginActivity.this, MainActivity.class));

        String url = "http://soa.sensecode.id/user/login/5d0ce2f1f3ad4365a62c93f4fc8bbefa";
        JSONObject data = new JSONObject();

        try {
            data.put("username",eduser.getText().toString());
            data.put("password", edpass.getText().toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest req = new JsonObjectRequest(
                Request.Method.POST, url, data,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getString("status").equals("true"))
                            {
                                userLogin = eduser.getText().toString();
                                startActivity(new Intent(LoginActivity.this, MainActivity.class));
                                Toast.makeText(getApplicationContext(), "Welcome, "+eduser.getText().toString(), Toast.LENGTH_LONG).show();
                            }else{
                                Toast.makeText(getApplicationContext(), response.getString("status"), Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_LONG).show();
            }
        }
        );

        rq.add(req);
    }
}
