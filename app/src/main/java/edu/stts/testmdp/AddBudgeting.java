package edu.stts.testmdp;

import android.arch.persistence.room.Room;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import java.util.List;

public class AddBudgeting extends AppCompatActivity {
    EditText maxbudget;
    RecyclerView recyclerView;
    RecyclerView.Adapter adapter;
    Button btn_add;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_budgeting);

        maxbudget = findViewById(R.id.ed_max_budget);
        btn_add = findViewById(R.id.btn_b_add);
        recyclerView = findViewById(R.id.rview_budget);

        setTitle("Budgeting");

        final DbBudget db = Room.databaseBuilder(getApplicationContext(), DbBudget.class, "dbbudget").allowMainThreadQueries().build();
        List<Budget> budgets = db.budgetDao().getAllBudget();

        if (budgets.size()>0)
        {
            recyclerView.setLayoutManager(new LinearLayoutManager(this));
            adapter = new BudgetAdapter(budgets);
            recyclerView.setAdapter(adapter);
        }

        btn_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                db.budgetDao().insertAll(new Budget(Integer.parseInt(maxbudget.getText().toString()), MainActivity.jumlah, MainActivity.walletid, LoginActivity.userLogin, MainActivity.namawallet));
            }
        });
    }
}
